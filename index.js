const {uuidv4} = require('@apexsoftwaresolutions/uuidv4');

// const emailAddress = `${email_id}+proton.me`

// console.info(email_id);

class Email {
    emailAddress = null;

    constructor() {
        this.emailAddress;
    }

    // Email Address
    get emailAddress() {
        return this.emailAddress;
    }

    set emailAddress(emailAddress) {
        this.emailAddress = emailAddress;
    }

    // Email Domain
    get emailDomain() {
        return this.emailAddress.split('@')[1];
    }

    set emailDomain(domain) {
        this.emailAddress = `${this.emailAddress.split('@')[0]}@${domain}`;
    }

    // Email Id
    // get emailId() {
    //     return this.emailAddress.split('@')[0];
    // }

    emailId(emailId) {
        this.emailAddress = `${emailId}@${this.emailAddress.split('@')[1]}`;
    }
}



function main() {
    const id = uuidv4();
    // const email = new Email();

    // const emailId = email.emailId(id);
    // const emailDomain = email.emailDomain = 'proton.me';
    // const emailAddress = email.emailAddress = `${email.emailId}@${email.emailDomain}`;
    console.log(`\nid: ${id}\n\nemail: ${id}@proton.me\n`)
    // console.info(`id: ${emailId}\ndomain: ${emailDomain}\nemail: ${emailAddress}`);
}

main();